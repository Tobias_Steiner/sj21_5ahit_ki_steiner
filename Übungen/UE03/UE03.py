#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import re
import nltk
import numpy as np
from nltk.tokenize import word_tokenize 
import spacy

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer


# ## 3.2

# In[2]:


tweets=[
 'This is introduction to NLP',
 'It is likely to be useful, to people ',
 'Machine learning is the new electrcity',
 'There would be less hype around AI and more action going forward',
 'python is the best tool!','R is good langauage',
 'I like this book','I want more books like this'
] 


# In[3]:


df = pd.DataFrame(tweets, columns=["tweets"])
df["tweets"] = df["tweets"].str.lower()
df


# ## 3.3

# In[4]:


df["tweets"] = df.tweets.str.replace(r"[^a-zA-Z0-9]"," ")
df


# ## 3.4

# In[5]:


#text="Hello there! Welcome to the programming world."
#print(nltk.word_tokenize(text))
#nltk.download('punkt')


# In[6]:


text = "This is the first sentence. This is the second one."
list_of_words = text.split()
print(list_of_words) 


# In[7]:


print(word_tokenize(text)) 


# ##### Der Unterschied zwischen den beiden Ausgaben ist, dass beim "word_tokenize"
# #####  jeden Token einzeln erkennt, z.B. unterscheidet es ein Satzzeichen von 
# #####  einem string. 
# #####  Bei .split() Wird das nicht unterschieden, weil man es selbst noch dazu 
# #####  definieren müsste

# ## 3.5

# In[8]:


#token = df["tweets"].str.split()
#token
df["tokenized"] = df["tweets"].apply(word_tokenize)
#word_tokenize(str(df["tweets"]))
df


# ## 3.6

# In[9]:


with open('dataScientistJobDesc.txt', encoding='utf-8') as f:
    data_text=f.read()


# In[10]:


tokenized_data = word_tokenize(data_text)
len(tokenized_data)


# In[11]:


#tokenized_data = np.unique(tokenized_data)
#len(dsj)
set_data = set(tokenized_data)
len(tokenized_data)


# ## 3.7

# In[12]:


nlp = spacy.load("de_core_news_sm")
doc = nlp("Apple erwägt den Kauf eines österreichischen Startups um 6 Mio. Euro.")
print(doc)


# In[13]:


for token in doc:
    print(f"{token.text:{20}}",type(token)) 


# In[14]:


with open('dataScientistJobDesc.txt', encoding='utf-8') as f:
    data_text = f.read()


# In[15]:


nlp = spacy.load("en_core_web_sm")
doc = nlp(data_text)
len(doc)


# In[16]:


set_doc = set()
seen = set()
for word in doc:
    if word.text not in seen:
        set_doc.add(word.text)
    seen.add(word.text)
len(set_doc)


# In[17]:


sorted(set_data)


# In[18]:


sorted(set_doc)


# In[19]:


difference_1 = set_data.difference(set_doc)
difference_2 = set_doc.difference(set_data)

print(difference_1)
print(difference_2)

list_difference = list(difference_1.union(difference_2))
print(list_difference)
len(list_difference)


# NLP unterscheidet keine Bindezeichen wie '-' oder '/' während Spacy keine Zeichen wie '`' und '\n'.

# ### Stoppwörter

# In[20]:


#nltk.download('stopwords')


# In[21]:


stop = stopwords.words('english')
text = "How to develop a chatbot in Python"
result = [word for word in text.split() if word not in stop]
result


# ## 3.8

# "How" wurde nicht entfernt, da es die Bedeutung eines Satzes komplett Veränderen kann.

# In[22]:


tweets=[
 'This is introduction to NLP',
 'It is likely to be useful, to people ',
 'Machine learning is the new electrcity',
 'There would be less hype around AI and more action going forward',
 'python is the best tool!','R is good langauage',
 'I like this book','I want more books like this'
]


# In[23]:


df = pd.DataFrame(tweets, columns=["tweets"])
df


# In[24]:


stop = stopwords.words('english')
tweets_result = []

for i in tweets:
    tweet = word_tokenize(i.lower())
    tweets_col = []
    for w in tweet:
        if w not in stop:
            tweets_col.append(w)
    tweets_result.append(tweets_col)

tweets_result = [" ".join(tweet) for tweet in tweets_result]
    

df = pd.DataFrame(tweets_result, columns=["tweets"])
df


# #### Stemming

# In[25]:


words = ['run','runner','running','ran','runs']
ps = PorterStemmer()
[ps.stem(w) for w in words]


# ### 3.10

# In[26]:


text=['I like fishing','I eat fish','There are many fishes in pound']
ps = PorterStemmer()
df = pd.DataFrame([ps.stem(w) for w in text])
df.columns = ['']

print(df)
df.dtypes


# #### Lemmatizition

# In[27]:


#nltk.download('wordnet')
lemmatizer = WordNetLemmatizer()
print("rocks:", lemmatizer.lemmatize("rocks"))
print("studies:", lemmatizer.lemmatize("studies"))
print("better :", lemmatizer.lemmatize("better", pos ="a"))


# ### 3.11

# In[28]:


text=['I like fishing','I eat fish','There are many fishes in pound']
lemmatizer = WordNetLemmatizer()
text_result = []

for w in text:
    text_col = []
    text_part = w.split()
    for t in text_part:
        text_col.append(lemmatizer.lemmatize(t))
    text_result.append(text_col)

text_result = [" ".join(t) for t in text_result]

df = pd.DataFrame(text_result, columns=[''])

print(df)
df.dtypes

