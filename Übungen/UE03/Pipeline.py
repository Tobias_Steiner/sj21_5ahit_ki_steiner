#!/usr/bin/env python
# coding: utf-8

# In[1]:


import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import re


# In[2]:


nltk.download('stopwords')
stop = stopwords.words('english')
nltk.download('wordnet')


# In[3]:


def preprocess_text(text_to_process, processing_function_list): #In dieser Funktion kann eine Text und eine Liste von 
    text = text_to_process                                      #Funktionen übergeben werden die dann ausgeführt wird
    for function in processing_function_list:
        text = function(text)
    return text


# In[4]:


def to_lower(text): #Text wird kleingeschrieben
    return text.lower()


# In[5]:


def remove_url(text): #Urls werden gelöscht
    return re.sub("https\S+", '', text)


# In[6]:


def remove_email(text): # Alle Emails werden gelöscht
    return re.sub("[\w\.-]+@[\w\.-]+", '', text)


# In[7]:


def remove_punctuation(text): #Alle Satzzeichen werden gelöscht
    return re.sub("[,.!?:]", '', text)


# In[8]:


def remove_stopword(text):
    tokens = word_tokenize(text) // Tokanisieren
    return (" ").join([w for w in tokens if not w in stopwords.words()]) # Stopwörter entfernen 
                                                                         #String durch Join zurückgeben


# In[9]:


def lemmatize_word(text): #Schreibt wörter in ihre Stammform
    lemmatizer = WordNetLemmatizer()
    word_list = nltk.word_tokenize(text)
    return ' '.join([lemmatizer.lemmatize(w) for w in word_list]) #Wörter in Stammform umschreiben 
                                                                  #String durch Join zurückgeben


# In[10]:


text = "remove: t.steiner@htlkrems.at, remove: Remove, remove: https://www.htlkrems.ac.at/" #Beispieltext
preprocess_functions = [to_lower, remove_url, remove_email, remove_punctuation] #Die Liste der Funktionen
preprocessed_text = preprocess_text(text, preprocess_functions)


# In[11]:


preprocessed_text

