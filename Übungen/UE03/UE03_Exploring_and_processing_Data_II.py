#!/usr/bin/env python
# coding: utf-8

# FreqList
# Counter
# 3.2.2 duplikate drin lassen, nix entfernen (3 übungen)

# In[1]:


import nltk

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

import spacy

from collections import Counter
import re


# # Exploring and processing Data II

# ### POS-Tagging mit NLTK

# In[2]:


#nltk.download('averaged_perceptron_tagger')


# In[3]:


text = "I love NLP and I will learn NLP in 2 month"
stop_words = set(stopwords.words('english'))

words = [token for token in word_tokenize(text) if token not in stop_words]

print(nltk.pos_tag(words))


# In[4]:


#nltk.download('tagsets')


# In[5]:


nltk.help.upenn_tagset()


# ### POS-Tagging mit Spacy

# In[6]:


nlp = spacy.load("en_core_web_sm")
doc = nlp("I love NLP and I will learn NLP in 2 month")

words=[token for token in doc if token.is_stop == False]

for token in words:
    print(token.text, token.pos_)


# ### 3.2.1

# #### Spacy

# In[7]:


with open('dataScientistJobDesc.txt', encoding='utf-8') as f:
    data_text = f.read()


# In[8]:


nlp = spacy.load("en_core_web_sm")
doc = nlp(data_text)

words = [token for token in doc if token.pos_ in ["NOUN", "PROPN"]]

len(words)


# #### NLTK

# In[9]:


nltk_words = [token for token in word_tokenize(data_text)]
words = [token[1] for token in nltk.pos_tag(nltk_words) if token[1] in ["NN","NNP","NNPS","NNS"]]

len(words)


# ### 3.2.2

# In[10]:


cnt = Counter()

for token in doc:
    cnt[token.pos_] += 1

print(cnt)


# ### 3.2.3

# In[11]:


words = re.findall(r'\w+', data_text.lower())
Counter(words).most_common(1)

