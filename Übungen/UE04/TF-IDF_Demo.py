#!/usr/bin/env python
# coding: utf-8

# In[1]:


doc1= "It is going to rain today."
doc2= "Today I am not going outside."
doc3= "I am going to watch the season premiere."

docs = [doc1, doc2, doc3]

print(docs)


# In[2]:


from sklearn.feature_extraction.text import TfidfVectorizer

vectorizer = TfidfVectorizer()


# In[3]:


X = vectorizer.fit_transform(docs)
# Werte werden normalisiert in einen Bereich zw. 0 und 1:
X.toarray()[0]


# In[4]:


analyze = vectorizer.build_analyzer()

print('Tokens des Docs 1',analyze(doc1))
print('Tokens des Docs 2',analyze(doc2))
print('Tokens des Docs 3',analyze(doc3))


# In[5]:


print("Unique Tokens aller Dokumente:")
print(vectorizer.get_feature_names_out())


# In[ ]:


print(vectorizer.vocabulary_)


# In[ ]:


# 1=niedrigste Wert für jene Wörter, die am häufigsten vorkommen (Ann.: z.B. the oder and, die ja nicht so wichtig sind!). 
# Das regelt die IDF 
dict(zip(vectorizer.get_feature_names_out(), vectorizer.idf_))


# In[ ]:


X.toarray().shape


# In[ ]:


#print(vectorizer.get_params())


# In[ ]:


vectorizer.transform(["Today it is raining"]).toarray()

