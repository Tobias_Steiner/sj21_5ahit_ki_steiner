#!/usr/bin/env python
# coding: utf-8

# In[6]:


import spacy
from sklearn.feature_extraction.text import CountVectorizer

from tensorflow.keras.utils import to_categorical


# # Converting Text to Features I

# In[7]:


nlp = spacy.load('en_core_web_sm')


# In[8]:


text="Jim loves NLP. He will learn NLP in two monts. NLP is future."

def get_spacy_tokens(text):
    doc = nlp(text)
    return [token.text for token in doc]

text_tokens = get_spacy_tokens(text)

vectorizer = CountVectorizer(tokenizer=get_spacy_tokens, lowercase=False)

# Erstellung des Vokabulars:
vectorizer.fit(text_tokens)
vocab = vectorizer.vocabulary_
print("Vocabulary: ", vocab)

# Erstellung der Matrix mit Wortvektoren (One Hote Encoding):
vector = vectorizer.transform(text_tokens)
print("Encoded Document is:")
print(vector.toarray())


# ### 4.1 

# In[9]:


def one_hot_encoding(sent):
    doc = nlp(sent)
    vect_num = []
    for token in doc:
        for key, value in vocab.items():
            if token.text == key:
                vect_num.append(value)
    return to_categorical(vect_num)


# In[10]:


print(one_hot_encoding("Jim will learn NLP in future"))


# In[ ]:




