#!/usr/bin/env python
# coding: utf-8

# In[1]:


from nltk.util import ngrams

from sklearn.feature_extraction.text import CountVectorizer 


# # Converting Text to Features II

# In[2]:


text = "I love NLP and I will learn NLP in two month "
N=2
bigrams = ngrams(text.split(), N)
for bigram in bigrams:
     print(bigram)


# In[3]:


N=3
bigrams = ngrams(text.split(), N)
for bigram in bigrams:
     print(bigram)


# In[4]:


vectorizer = CountVectorizer(ngram_range=(2,2))
vectorizer.fit([text])
vectorizer.vocabulary_ 


# ### 4.2.1

# 1) - love nlp
# 
# 2) - two month
# 
# 3) - nlp in

# ### 4.2.2

# In[6]:


def ngram_trump(n):
    df_trump = pd.read_csv("realdonaldtrump.csv") #Dataset auslesen
    content=df_trump["content"].array #Contentspalte aus dataframe auslesen
    
    stop_words = set(stopwords.words('english')) #Stopwörter einlesen
    
    tokenized_words= [nltk.word_tokenize(sent)for sent in content] #für jeden Satz die Wörter tokenizen
    tokenized_words_al= [[x for x in ls if x.isalnum()] for ls in tokenized_words] #Remove punctuation (non alphagemeric)
    
    trump_tokens = [[x.lower() for x in ls if not x.lower() in stop_words]for ls in tokenized_words_al] #token in lowercase und stopwords entfernen
    
    bigrams_trump=[ngrams(t,n) for t in trump_tokens] #biagram erstellen
    bigrams_trump_new = [item for sublist in bigrams_trump for item in sublist] #biagram array unwinden
    
    c = Counter(bigrams_trump_new) #counter für häufigsten biagrams erstellen
    
    return c.most_common(3)


# In[ ]:




