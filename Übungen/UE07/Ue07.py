#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import nltk
nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer
sid = SentimentIntensityAnalyzer()
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay


# In[ ]:


#7.1
a = 'This was a good movie.'
sid.polarity_scores(a)


# In[ ]:


a = 'This was the best, most awesome movie EVER MADE!!!'
sid.polarity_scores(a)


# In[ ]:



df = pd.read_csv('areviews.tsv', sep='\t')
df.head()


# In[ ]:


df['label'].value_counts()


# In[ ]:


# REMOVE NaN VALUES AND EMPTY STRINGS:
df.dropna(inplace=True)

blanks = []  # start with an empty list

for i,lb,rv in df.itertuples():  
    if type(rv)==str:            
        if rv.isspace():        
            blanks.append(i)     

df.drop(blanks, inplace=True)


# In[ ]:


df['scores'] = df['review'].apply(lambda review: sid.polarity_scores(review))

df.head()


# In[ ]:


df['compound']  = df['scores'].apply(lambda score_dict: score_dict['compound'])

df.head()


# In[ ]:


df['comp_score'] = df['compound'].apply(lambda c: 'pos' if c >=0 else 'neg')

df.head()


# In[ ]:


# Write a review as one continuous string (multiple sentences are ok)
review = 'The shoes I brought were amazing.'
# Obtain the sid scores for your review
sid.polarity_scores(review)


# In[ ]:


review='The mobile phone I bought was the WORST and very BAD'
# Obtain the sid scores for your review
sid.polarity_scores(review)


# In[ ]:


#7.2
#Report
report=classification_report(df["label"], df["comp_score"])
print(report)


# In[ ]:


#Confusion-Matrix
matrix=confusion_matrix(df["label"], df["comp_score"])
disp=ConfusionMatrixDisplay(matrix, display_labels=["pos","neg"])
disp.plot()


# In[ ]:


#7.3
df_film = pd.read_csv('moviereviews.tsv', sep='\t')
# REMOVE NaN VALUES AND EMPTY STRINGS:
df_film.dropna(inplace=True)

blanks = []  # start with an empty list

for i,lb,rv in df_film.itertuples():  
    if type(rv)==str:            
        if rv.isspace():        
            blanks.append(i)     

df_film.drop(blanks, inplace=True)
df_film['scores'] = df_film['review'].apply(lambda review: sid.polarity_scores(review))
df_film['compound']  = df_film['scores'].apply(lambda score_dict: score_dict['compound'])
df_film['comp_score'] = df_film['compound'].apply(lambda c: 'pos' if c >=0 else 'neg')


# In[ ]:


report_film=classification_report(df_film["label"], df_film["comp_score"])
print(report_film)


# In[ ]:


#Confusion-Matrix
matrix_film=confusion_matrix(df_film["label"], df_film["comp_score"])
disp_film=ConfusionMatrixDisplay(matrix_film, display_labels=["pos","neg"])
disp_film.plot()

