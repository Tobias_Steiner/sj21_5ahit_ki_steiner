#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression


# In[2]:


#Read DF and drop NA
df_complaints = pd.read_csv("consumer_complaints.csv", usecols =["product","consumer_complaint_narrative"])
df_complaints.dropna(inplace=True)
df_complaints


# In[3]:


#Step2
df_complaints["product"].value_counts()


# In[4]:


#Step3
X_train, X_test, y_train, y_test = train_test_split(
    df_complaints["consumer_complaint_narrative"], df_complaints["product"], test_size=0.33, random_state=42)


# In[5]:


print(f"{X_train.shape} {X_test.shape} {y_train.shape} {y_test.shape}")


# In[6]:


#Step 4
le = LabelEncoder()
le.fit(df_complaints["product"].unique())
y_train=le.transform(y_train)
y_test=le.transform(y_test)


# In[7]:


vectorizer = TfidfVectorizer(analyzer='word', max_features=2000)
vectorizer.fit(X_train)
X_train=vectorizer.transform(X_train)
X_test=vectorizer.transform(X_test)


# In[8]:


print(f"{X_train.shape} {X_test.shape}")


# In[9]:


#Step5
clf = LogisticRegression()
clf.fit(X_train,y_train)


# In[10]:


y_pred=clf.predict(X_test)


# In[11]:


accuracy_score(y_test, y_pred)

